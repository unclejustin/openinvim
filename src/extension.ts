import * as vscode from "vscode";

export function activate(context: vscode.ExtensionContext) {
  let disposable = vscode.commands.registerCommand("openinvim.openFile", () => {
    const currentFile = vscode.window.activeTextEditor as vscode.TextEditor;
    const fileName = currentFile.document.fileName;
    const isSaved = Boolean(!fileName.match(/untitled/i));
    const vimTerminal = vscode.window.createTerminal("Vim");

    if (isSaved) {
      // Open current file in vim
      vimTerminal.sendText(`vim ${fileName}`, true);
    } else {
      // Copy contents of current editor to clipboard then open vim
      const contents = currentFile.document.getText();
      vscode.env.clipboard.writeText(contents);
      vscode.window.setStatusBarMessage(
        `Contents of editor copied to clipboard.`,
        3000
      );
      vimTerminal.sendText(`vim`, true);
    }

    // Show the terminal panel and maximize it
    vimTerminal.show();
    vscode.commands.executeCommand("workbench.action.toggleMaximizedPanel");
  });

  context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() {}
