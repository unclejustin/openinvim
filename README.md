# Open in Vim

Love VS Code and Vim, but the Vim plugins just don't do it for you?

Sames. This extension simply opens whatever file you currently have active in the integrated terminal with Vim. That's it. A simple shortcut to jump over to Vim without leaving VS Code 🎉

## Features

Open the file in your active editor in the integrated terminal with Vim.

![Open existing file](https://gitlab.com/unclejustin/openinvim/raw/master/images/openinvim.gif)

Copy unsaved text to the clipboard then open up Vim.

![Copy unsaved text](https://gitlab.com/unclejustin/openinvim/raw/master/images/copytext.gif)
